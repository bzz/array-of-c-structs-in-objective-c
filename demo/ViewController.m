//
//  ViewController.m
//  demo
//
//  Created by Mikhail Baynov on 29/08/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"

typedef struct {
    int x;
    int y;
}
TheStruct;


#define ARR_SIZE 3


@interface ViewController ()
{}
@property (nonatomic, strong) NSMutableData *allData;


@end

@implementation ViewController

- (NSString *)documentsDirectory
{
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    //init data
    TheStruct arr[ARR_SIZE];
    [self.allData getBytes:&arr length:ARR_SIZE*sizeof(TheStruct)];
    NSLog(@" INITIALISED DATA:"); for (size_t i=0; i<ARR_SIZE; ++i) {[self logTheStruct:arr[i]];}
    
    
    
    [self loadAllData];
    
    //set values
    TheStruct a = {10, 12};
    TheStruct b = {.x = 50, .y = 60};
    TheStruct c;
    c.x = 104;
    c.y = 120;
    
    arr[0] = a;
    arr[1] = b;
    arr[2] = c;
    
    NSLog(@" VALUES SET FOR DATA:"); for (size_t i=0; i<ARR_SIZE; ++i) {[self logTheStruct:arr[i]];}
    
    //save
    self.allData = [NSMutableData dataWithBytes:&arr length:ARR_SIZE*sizeof(TheStruct)];
    [self.allData writeToFile:[self.documentsDirectory stringByAppendingPathComponent:@"fileName.dat"] atomically:YES];
    
    
}

- (NSMutableData *)allData
{
    if (!_allData) {
        _allData = [NSMutableData new];
        for (int i=0; i<ARR_SIZE; ++i) {
            TheStruct tmpStruct = {0};
            NSData *dataToAppend = [NSData dataWithBytes:&tmpStruct length:sizeof(TheStruct)];
            [dataToAppend getBytes:&tmpStruct length:sizeof(TheStruct)];
            [self.allData appendData:dataToAppend];
        }
    }
    return _allData;
}



- (void)logTheStruct:(TheStruct)s
{
    NSLog(@"x = %i    y = %i", s.x, s.y);
}


- (void)loadAllData
{
    self.allData = [NSMutableData dataWithContentsOfFile:[self.documentsDirectory stringByAppendingPathComponent:@"fileName.dat"]];
    
    TheStruct arr[ARR_SIZE];
    [self.allData getBytes:&arr length:self.allData.length*sizeof(TheStruct)];
    ///    TheStruct* arr = (TheStruct*)self.allData.mutableBytes;     //alternative??
    
    NSLog(@" LOADED DATA:"); for (size_t i=0; i<ARR_SIZE; ++i) {[self logTheStruct:arr[i]];}
}


@end
