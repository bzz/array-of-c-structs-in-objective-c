//
//  main.m
//  demo
//
//  Created by Mikhail Baynov on 29/08/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
